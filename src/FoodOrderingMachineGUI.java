import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class FoodOrderingMachineGUI {
    private JButton tempuraButton;
    private JButton udonButton;
    private JButton karaageButton;
    private JButton yakisobaButton;
    private JButton gyozaButton;
    private JButton ramenButton;
    private JTextPane textPane1;
    private JButton checkOutButton;
    private JPanel root;
    private JLabel sum;

    int total=0;

    void order(String food,int price){
        int confirmation = JOptionPane.showConfirmDialog(
                null,
                "Would you like to order "+food+"?",
                "Order Confirmation",
                JOptionPane.YES_NO_OPTION
        );
        if(confirmation == 0) {
            JOptionPane.showMessageDialog(
                    null,
                    "Thank you for ordering " + food + "! It will be served as soon as possible"
            );
            total += price;
            sum.setText("" + total + " yen");
            textPane1.replaceSelection(""+food+price+"yen\n");
        }
    }

    void checkOut(String action){
        int confirmation = JOptionPane.showConfirmDialog(
                null,
                "Would you like to "+action+" ?",
                "Check Out Confirmation",
                JOptionPane.YES_NO_OPTION
        );
        if(confirmation == 0){
            JOptionPane.showMessageDialog(
                    null,
                    "Thank you. The total price is "+ total +" yen"
            );
            total= 0;
            sum.setText(""+ total + " yen");
            textPane1.setText("");
        }
    }
    public FoodOrderingMachineGUI() {

        tempuraButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Tempura",120);
            }
        });
        tempuraButton.setIcon(new ImageIcon( this.getClass().getResource("tempura.jpg")
        ));
        karaageButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Karaage",130);
            }
        });
        karaageButton.setIcon(new ImageIcon( this.getClass().getResource("karaage.jpg")
        ));

        gyozaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Gyoza",140);
            }
        });
        gyozaButton.setIcon(new ImageIcon( this.getClass().getResource("gyoza.jpg")
        ));

        udonButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Udon",150);
            }
        });
        udonButton.setIcon(new ImageIcon( this.getClass().getResource("udon.png")
        ));

        yakisobaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Yakisoba",160);
            }
        });
        yakisobaButton.setIcon(new ImageIcon( this.getClass().getResource("yakisoba.jpg")
        ));

        ramenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Ramen",180);
            }
        });
        ramenButton.setIcon(new ImageIcon( this.getClass().getResource("ramen.jpg")
        ));

        checkOutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                checkOut("check out");
            }
        });
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("FoodOrderingMachineGUI");
        frame.setContentPane(new FoodOrderingMachineGUI().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}
